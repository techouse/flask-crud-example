# Python Flask CRUD example

## Requirements
* Python __3.4+__
* virtualenv
* pip
* MySQL __5.5+__

## How to get started
First create a MySQL database and user to access that database. Then run the following commands in this sequence:
```bash
virtualenv-3 env
source env/bin/activate
pip3 install -r requirements.txt
cp instance/config.py.example instance/config.py
vim instance/config.py
```
Now enter the database credentials into the config.py file and then run these commands in succession:
```bash
export FLASK_APP=run.py
export FLASK_CONFIG=development
flask db init
flask db migrate
flask db upgrade
flask run
```
Now you can visit your Flask app the address http://localhost:5000